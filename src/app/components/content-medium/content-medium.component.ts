import { Component, OnInit } from '@angular/core';
import { Information } from '../models/Interface-information';

@Component({
  selector: 'app-content-medium',
  templateUrl: './content-medium.component.html',
  styleUrls: ['./content-medium.component.scss']
})
export class ContentMediumComponent implements OnInit {
  public lessons: Information  [] ;
  constructor() {
    this.lessons = [
      {
        logo: '../../assets/img/pump.svg',
        training: 'BodyPump',
        aditional: {
          calories: 120,
          muscles: [`Shoudlder, Legs, Buttocks`],
          teacher: 'Michael',
        }
      },
      {
        logo: '../../assets/img/attack.svg',
        training: 'Body Combat',
        aditional: {
          calories: 150,
          muscles: [`Arms, Legs, Calves`],
          teacher: 'Tatiana',
        }
      },
      {
        logo: '../../assets/img/yoga.svg',
        training: 'Yoga',
        aditional: {
          calories: 20,
          muscles: ['All Body'],
          teacher: 'Ronces',
        }
      },
      {
        logo: '../../assets/img/ejercicio.svg',
        training: 'Zumba',
        aditional: {
          calories: 20,
          muscles: [`Chest, Legs, Head, Fingers, Toes`],
          teacher: 'Ronces',
        }
      },
      {
        logo: '../../assets/img/anillos-de-gimnasia.svg',
        training: 'Lorem',
        aditional: {
          calories: 40,
          muscles: [`Chest, Gluteos, Piernas`],
          teacher: 'Ronces',
        }
      },
      {
        logo: '../../assets/img/patada-de-taekwondo.svg',
        training: 'Lorem Ipsum',
        aditional: {
          calories: 32,
          muscles: [`Chest, Caderas,Piernas`],
          teacher: 'Ronces',
        }
      },
    ];
    }

  ngOnInit(): void {
  }

}
