import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentMediumComponent } from './content-medium.component';

describe('ContentMediumComponent', () => {
  let component: ContentMediumComponent;
  let fixture: ComponentFixture<ContentMediumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentMediumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
