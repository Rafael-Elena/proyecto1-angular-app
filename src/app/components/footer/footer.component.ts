import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public linkedin: string;
  public twitter: string;
  public facebook: string;
  public instagram: string;

  constructor() {
    this.linkedin = '../../assets/img/linkedin.svg';
    this.twitter = '../../assets/img/twitter.svg';
    this.facebook = '../../assets/img/facebook.svg';
    this.instagram = '../../assets/img/instagram.svg';
   }

  ngOnInit(): void {
  }

}
