import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public logo: string;
  public home: string;
  public about: string;
  public signin: string;
  public profile: string;

  constructor() {
    this.logo = '../../assets/img/logotipo-de-xing.svg';
    this.home = 'Home';
    this.about = 'About';
    this.signin = ' Sign';
    this.profile = 'Profile';
    console.log(this.logo);
   }

  ngOnInit(): void {
  }

}
