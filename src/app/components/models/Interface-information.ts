export interface Information {
    image?: string;
    training?: string;
    room?: string;
    level?: number;
    aditional?: AditionalInformation;
    logo?: string;
}

export interface AditionalInformation {
    calories: number;
    muscles: string[];
    teacher: string;
}