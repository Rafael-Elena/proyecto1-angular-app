import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentSimpleComponent } from './content-simple.component';

describe('ContentSimpleComponent', () => {
  let component: ContentSimpleComponent;
  let fixture: ComponentFixture<ContentSimpleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentSimpleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
