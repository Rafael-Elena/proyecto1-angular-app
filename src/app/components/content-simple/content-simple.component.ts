
import { Component, OnInit } from '@angular/core';
import { Information } from '../models/Interface-information';

@Component({
  selector: 'app-content-simple',
  templateUrl: './content-simple.component.html',
  styleUrls: ['./content-simple.component.scss']
})
export class ContentSimpleComponent implements OnInit {
  public lessons: Information  [] ;
  constructor() {
    this.lessons = [
      {
        image: '../../assets/img/01.png',
        training: 'BodyPump',
        room: 'Room 1',
        level: 10,
      },
      {
        image: '../../assets/img/02.png',
        training: 'Body Combat',
        room: 'Room 2',
        level: 8,
      },
      {
        image: '../../assets/img/03.png',
        training: 'Yoga',
        room: 'Room 3',
        level: 6,
      },
      {
        image: '../../assets/img/04.png',
        training: 'Crossfit',
        room: 'Room 4',
        level: 12,
      },
      {
        image: '../../assets/img/05.png',
        training: 'Zumba',
        room: 'Room 5',
        level: 7,
      },
      {
        image: '../../assets/img/06.png',
        training: 'Box',
        room: 'Room 5',
        level: 15,
      },
      {
        image: '../../assets/img/07.png',
        training: 'Box',
        room: 'Room 5',
        level: 15,
      },
      {
        image: '../../assets/img/08.png',
        training: 'Box',
        room: 'Room 5',
        level: 13,
      },
    ];
    }

  ngOnInit(): void {
  }

}
